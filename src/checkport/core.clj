(ns checkport.core
  (:gen-class)
  (:require
    [clojure.string :as str]
    [clojure.java.io :as jio]
    [clojure.tools.cli :as cli])
  (:import
    [java.net Socket]))

(def cli-options
  [["-h" "--help"]])

(defn- usage [options-summary]
  (->> ["Usage: program-name < input-file.txt"
        "       each entry in the file is of the form:"
        "       host1 port1"
        "       host2 port2"
        "Options:"
        options-summary]
       (str/join \newline)))

(defn- check-each [[host port]]
  (let [h host
        p (Integer/parseInt port)
        response {:host h :port p}]
    (try
      (let [socket (Socket. h p)]
       (.close socket)
       (assoc response :open true))
      (catch Exception e
        (assoc response :open false)))))

(defn process-input
  [reader]
  (with-open [rdr reader]
    (doseq [line (line-seq rdr)]
      (when (not-empty (str/trim line))
        (println (check-each (str/split line #" ")))))))

(defn- exit [status msg]
  (println msg)
  (System/exit status))

(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (cli/parse-opts args cli-options)
        reader (jio/reader *in*)]
    (cond
      (:help options) (exit 0 (usage summary))
      errors (exit 1 (str/join errors))
      :else (process-input reader))))